#!/bin/sh

pkgver=95.0.1
objdir=obj-*/dist/librewolf
wasi_path=/usr/local/Cellar/llvm/13.0.0_2/lib/clang/13.0.0/lib
ospkg=app
bold=$(tput bold)
normal=$(tput sgr0)


fetch() {
    
    echo "\n${bold}-> Fetching Firefox source code${normal}"
    wget -q --show-progress https://archive.mozilla.org/pub/firefox/releases/$pkgver/source/firefox-$pkgver.source.tar.xz
    if [ $? -ne 0 ]; then exit 1; fi
    if [ ! -f firefox-$pkgver.source.tar.xz ]; then exit 1; fi
    echo "${bold}-> Retrieved firefox-$pkgver.source.tar.xz ${normal}"

}

extract() {

    echo "\n${bold}-> Extracting firefox-$pkgver.source.tar.xz (might take a while)${normal}"
    tar xf firefox-$pkgver.source.tar.xz
    if [ $? -ne 0 ]; then exit 1; fi
    if [ ! -d firefox-$pkgver ]; then exit 1; fi
    echo "${bold}-> Extracted successfully ${normal}"

}

apply_patches() {

    echo "\n${bold}-> Creating mozconfig${normal}"
    if [ ! -d firefox-$pkgver ]; then exit 1; fi
    cd firefox-$pkgver
    
    cat >../mozconfig <<END
ac_add_options --enable-application=browser

# This supposedly speeds up compilation (We test through dogfooding anyway)
ac_add_options --disable-tests
ac_add_options --disable-debug

ac_add_options --enable-release
ac_add_options --enable-hardening
ac_add_options --enable-rust-simd
ac_add_options --enable-optimize
ac_add_options --with-wasi-sysroot=$HOME/.mozbuild/wrlb/wasi-sysroot

# Branding
ac_add_options --enable-update-channel=release
ac_add_options --with-app-name=librewolf
ac_add_options --with-app-basename=LibreWolf
ac_add_options --with-branding=browser/branding/librewolf
ac_add_options --with-distribution-id=io.gitlab.librewolf-community
ac_add_options --with-unsigned-addon-scopes=app,system
ac_add_options --allow-addon-sideload

# Features
ac_add_options --disable-crashreporter
ac_add_options --disable-updater

# Disables crash reporting, telemetry and other data gathering tools
mk_add_options MOZ_CRASHREPORTER=0
mk_add_options MOZ_DATA_REPORTING=0
mk_add_options MOZ_SERVICES_HEALTHREPORT=0
mk_add_options MOZ_TELEMETRY_REPORTING=0

END

    echo "${bold}-> Applying Librewolf patches${normal}"
    patch -p1 -i ../common/patches/allow-ubo-private-mode.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/about-dialog.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/context-menu.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/megabar.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/mozilla-vpn-ad.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/remove_addons.patch
    if [ $? -ne 0 ]; then exit 1; fi
    cp -vf ../patches/search-config.json services/settings/dumps/main/search-config.json
    patch -p1 -i ../common/patches/urlbarprovider-interventions.patch
    if [ $? -ne 0 ]; then exit 1; fi
    # patch -p1 -i ../patches/librewolf-pref-pane.patch
    # if [ $? -ne 0 ]; then exit 1; fi

    patch -p1 -i ../common/patches/sed-patches/allow-searchengines-non-esr.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/sed-patches/disable-pocket.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/sed-patches/remove-internal-plugin-certs.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/sed-patches/stop-undesired-requests.patch
    if [ $? -ne 0 ]; then exit 1; fi
    echo "${bold}-> Patches applied successfully${normal}"

    if ([[ -d ~/.mozbuild/macos-sdk/MacOSX11.3.sdk ]])
    then
        echo "ac_add_options --with-macos-sdk=$HOME/.mozbuild/macos-sdk/MacOSX11.3.sdk" >> ../mozconfig
        echo "${bold}-> Using SDK from .mozbuild${normal}"
    fi

    cd ..

}

prepare_wasi() {

    echo "\n${bold}-> Fetching the sdk${normal}"
    wget -q --show-progress https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-14/wasi-sdk-14.0-macos.tar.gz
    if [ $? -ne 0 ]; then exit 1; fi
    echo "\n${bold}-> Extracting the sdk${normal}"
    tar xf wasi-sdk-14.0-macos.tar.gz
    if [ $? -ne 0 ]; then exit 1; fi
    mkdir $HOME/.mozbuild/wrlb
    mkdir $wasi_path/wasi
    cp -r wasi-sdk-14.0/share/wasi-sysroot $HOME/.mozbuild/wrlb/wasi-sysroot
    cp -v wasi-sdk-14.0/lib/clang/13.0.0/lib/wasi/libclang_rt.builtins-wasm32.a $wasi_path/wasi/
    echo "${bold}-> LibreWolf can now include RLbox${normal}"
    
}

xcomp() {

    echo "ac_add_options --target=aarch64" >> mozconfig
    echo "${bold}-> Prepared to cross-compile${normal}"

}

branding() {

    cd firefox-$pkgver

    cp -r ../common/source_files/* ./
    patch -p1 -i ../patches/browser-branding-configure.patch
    if [ $? -ne 0 ]; then exit 1; fi
    cp -f ../icons/icns/Librewolf-3.icns browser/branding/librewolf/firefox.icns
    cp -f ../icons/png/Librewolf-3.png browser/branding/librewolf/content/about.png
    patch -p1 -i ../common/patches/browser-confvars.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/ui-patches/add-language-warning.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/ui-patches/pref-naming.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/ui-patches/remove-branding-urlbar.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/ui-patches/remove-cfrprefs.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/ui-patches/remove-organization-policy-banner.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/ui-patches/remove-snippets-from-home.patch
    if [ $? -ne 0 ]; then exit 1; fi
    patch -p1 -i ../common/patches/ui-patches/sanitizing-description.patch
    if [ $? -ne 0 ]; then exit 1; fi
    echo "${bold}-> Rebranded Librewolf and patched UI${normal}\n"

    cd ..
    echo "${bold}-> READY TO BUILD${normal}\n"

}


build() {

    echo "\n${bold}-> OK, let's build.${normal}\n"
    if [ ! -d firefox-$pkgver ]; then exit 1; fi
    cd firefox-$pkgver

    cp -v ../mozconfig .
    
    ./mach build
    if [ $? -ne 0 ]; then exit 1; fi
    
    cd ..
    echo "\n${bold}-> The build ended successfully${normal}\n"

}

package() {

    if [ ! -d firefox-$pkgver ]; then exit 1; fi
    cd firefox-$pkgver
    ./mach package
    if [ $? -ne 0 ]; then exit 1; fi
    echo "${bold}-> Packaged LibreWolf${normal}"

    cp -r $objdir ..
    cd ..
    cp -r settings/* librewolf/LibreWolf.$ospkg/Contents/Resources
    rm -rf librewolf/LibreWolf.$ospkg/Contents/Resources/docs
    rm librewolf/LibreWolf.$ospkg/Contents/Resources/LICENSE.txt
    rm librewolf/LibreWolf.$ospkg/Contents/Resources/README.md
    echo "${bold}-> Set LibreWolf settings${normal}"
    rm librewolf/LibreWolf.$ospkg/Contents/MacOS/pingsender

    echo "\n${bold}-> LibreWolf is now fully configured${normal}\n"

}

add_to_apps() {

    echo "\n${bold}-> Creating .zip${normal}"
    cp -v readme.md librewolf
    zip -qr9 librewolf-$pkgver.zip librewolf
    if [ $? -ne 0 ]; then exit 1; fi

    rm librewolf/readme.md
    cp -r librewolf/* /Applications    
    cp -v librewolf-$pkgver.zip ~/Downloads

    echo "\n${bold}-> LibreWolf.app available in /Applications\n"

}

cleanup() {

    echo "${bold}-> Cleaning (migth take a while)${normal}"
    rm mozconfig
    rm -rf firefox-* librewolf
    echo "${bold}-> All leftovers now gone${normal}"

}

full() {
    fetch
    extract
    apply_patches
    branding
    build
    package
}

full_x () {
    fetch
    extract
    apply_patches
    xcomp
    branding
    build
    package
}

# process commandline arguments and do something
done_something=0
if [[ "$*" == *fetch* ]]; then
    fetch
    done_something=1
fi
if [[ "$*" == *extract* ]]; then
    extract
    done_something=1
fi
if [[ "$*" == *apply_patches* ]]; then
    apply_patches
    done_something=1
fi
if [[ "$*" == *branding* ]]; then
    branding
    done_something=1
fi
if [[ "$*" == *build* ]]; then
    build
    done_something=1
fi
if [[ "$*" == *package* ]]; then
    package
    done_something=1
fi
if [[ "$*" == *full* ]]; then
    full
    done_something=1
fi
if [[ "$*" == *full_x* ]]; then
    full_x
    done_something=1
fi
if [[ "$*" == *add_to_apps* ]]; then
    add_to_apps
    done_something=1
fi
if [[ "$*" == *cleanup* ]]; then
    cleanup
    done_something=1
fi
if [[ "$*" == *xcomp* ]]; then
    xcomp
    done_something=1
fi
if [[ "$*" == *prepare_wasi* ]]; then
    prepare_wasi
    done_something=1
fi

if (( done_something == 0 )); then
    cat <<EOF

Build script for the OSX version of LibreWolf.
For more details check the build guide: https://gitlab.com/librewolf-community/browser/macos/-/blob/master/build_guide.md

${bold}BUILD${normal}

    ${bold}./build.sh${normal} command

${bold}BUILD COMMANDS${normal}

    ${bold}full${normal}
        The full build process.
    
    ${bold}fetch${normal}
        Fetches the source code.

    ${bold}extract${normal}
        Extracts the source code.
    
    ${bold}apply_patches${normal}
        Applies LibreWolf patches.
    
    ${bold}branding${normal}
        Applies LibreWolf branding and patches the UI.
    
    ${bold}build${normal}
        ./mach build.
    
    ${bold}package${normal}
        ./mach package and then applies the settings.

${bold}OTHER COMMANDS${normal}

    ${bold}add_to_apps${normal}
        After the build it can be used to create a .zip and then move LibreWolf.app to /Applications.

    ${bold}cleanup${normal}
        Removes leftovers from previous build processes.

    ${bold}xcomp${normal}
        Specify you want to build for aarm64 on x86 machines.

    ${bold}full_x${normal}
        Complete build process for aarm64 on x86 machines.
    
EOF
    exit 1
fi
